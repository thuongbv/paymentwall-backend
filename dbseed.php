<?php
require 'bootstrap.php';

$statement = <<<EOS
    CREATE TABLE IF NOT EXISTS users (
        id INT NOT NULL AUTO_INCREMENT,
        full_name VARCHAR(300) NOT NULL,
        email VARCHAR(300) NOT NULL UNIQUE,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;

    CREATE TABLE IF NOT EXISTS payment_system_accounts (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(300) NOT NULL,
        user_id INT DEFAULT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (id),
        FOREIGN KEY (user_id)
            REFERENCES users(id)
            ON DELETE SET NULL
    ) ENGINE=INNODB;

    INSERT INTO users
        (id, full_name, email)
    VALUES
        (1, 'Leoni Arnold', 'ylarasantia@reiprivre.tk'),
        (2, 'Brady Pugh', 'zahmedhamdey199@granurim.ml'),
        (3, 'Amir Armitage', 'litxsadiyaa@kukuka.org');

    INSERT INTO payment_system_accounts
        (name, user_id)
    VALUES
        ('dunn', 1),
        ('sutton', 1),
        ('hooper', 2);
EOS;

try {
    $createTable = $db_connection->exec($statement);
    echo "Success!\n";
} catch (\PDOException $e) {
    exit($e->getMessage());
}
?>