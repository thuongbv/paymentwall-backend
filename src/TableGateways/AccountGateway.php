<?php
namespace Src\TableGateways;

use Src\System\UtilityFunction;

class AccountGateway {

    private $db = null;
    private $userId = null;
    private $tableName = "payment_system_accounts";

    public function __construct($db, $userId)
    {
        $this->db = $db;
        $this->userId = $userId;
    }

    public function findAll()
    {
        $statement = "SELECT * FROM {$this->tableName} WHERE user_id = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($this->userId));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($id)
    {
        $statement = "SELECT * FROM {$this->tableName} WHERE id = ? AND user_id = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id, $this->userId));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(Array $input)
    {
        $statement = "INSERT INTO {$this->tableName} (`name`, user_id) VALUES (:name, :user_id);";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'name' => $input['name'],
                'user_id' => $this->userId
            ));
            UtilityFunction::saveLog($this->tableName, 'user id = '.$this->userId.' add record '.json_encode($input));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function update($id, Array $input)
    {
        $statement = "UPDATE {$this->tableName} SET `name` = :name WHERE id = :id AND user_id = :user_id;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'id' => (int) $id,
                'user_id' => (int) $this->userId,
                'name' => $input['name']
            ));
            UtilityFunction::saveLog($this->tableName, 'user id = '.$this->userId.' update record id = '.$id.', data: '.json_encode($input));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function delete($id)
    {
        $statement = "DELETE FROM {$this->tableName} WHERE id = :id AND user_id = :user_id;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array('id' => $id, 'user_id' => $this->userId));
            UtilityFunction::saveLog($this->tableName, 'user id = '.$this->userId.' delete record id = '.$id);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}