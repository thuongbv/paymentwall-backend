<?php
namespace Src\TableGateways;

use Src\System\UtilityFunction;

class UserGateway {

    private $db = null;
    private $tableName = "users";

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findAll()
    {
        $statement = "SELECT * FROM {$this->tableName};";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($id)
    {
        $statement = "SELECT * FROM {$this->tableName} WHERE id = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(Array $input)
    {
        $statement = "INSERT INTO {$this->tableName} (full_name, email) VALUES (:full_name, :email);";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'full_name' => $input['full_name'],
                'email'  => $input['email']
            ));
            UtilityFunction::saveLog($this->tableName, 'add record '.json_encode($input));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function update($id, Array $input)
    {
        $statement = "UPDATE {$this->tableName} SET full_name = :full_name, email  = :email WHERE id = :id;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'id' => (int) $id,
                'full_name' => $input['full_name'],
                'email'  => $input['email']
            ));
            UtilityFunction::saveLog($this->tableName, 'update record id = '.$id.', data: '.json_encode($input));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function delete($id)
    {
        $statement = "DELETE FROM {$this->tableName} WHERE id = :id;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array('id' => $id));
            UtilityFunction::saveLog($this->tableName, 'delete record id = '.$id);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}