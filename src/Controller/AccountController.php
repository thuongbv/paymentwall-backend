<?php
namespace Src\Controller;

use Src\TableGateways\AccountGateway;

class AccountController {

    private $db;
    private $requestMethod;
    private $accountId;
    private $userId;

    private $accountGateway;

    public function __construct($db, $requestMethod, $accountId, $userId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->accountId = $accountId;

        $this->accountGateway = new AccountGateway($db, $userId);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->accountId) {
                    $response = $this->getAccount($this->accountId);
                } else {
                    $response = $this->getAllAccounts();
                };
                break;
            case 'POST':
                $response = $this->createAccountFromRequest();
                break;
            case 'PUT':
                $response = $this->updateAccountFromRequest($this->accountId);
                break;
            case 'DELETE':
                $response = $this->deleteAccount($this->accountId);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllAccounts()
    {
        $result = $this->accountGateway->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_NUMERIC_CHECK);
        return $response;
    }

    private function getAccount($id)
    {
        $result = $this->accountGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_NUMERIC_CHECK);
        return $response;
    }

    private function createAccountFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateAccount($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->accountGateway->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = json_encode(['status' => 201]);
        return $response;
    }

    private function updateAccountFromRequest($id)
    {
        $result = $this->accountGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateAccount($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->accountGateway->update($id, $input);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode(['status' => 200]);
        return $response;
    }

    private function deleteAccount($id)
    {
        $result = $this->accountGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->accountGateway->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode(['status' => 200]);
        return $response;
    }

    private function validateAccount($input)
    {
        if (! isset($input['name'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = json_encode(['status' => 404]);
        return $response;
    }
}
