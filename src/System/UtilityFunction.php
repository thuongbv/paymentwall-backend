<?php
namespace Src\System;

class UtilityFunction {
    
    static function saveLog($table, $content)
    {
        if (!file_exists('../logs')) {
            mkdir('../logs', 0777, true);
        }

        file_put_contents("../logs/{$table}.txt", $content.' at '.date('Y-m-d H:i:s').PHP_EOL, FILE_APPEND);
    }
}