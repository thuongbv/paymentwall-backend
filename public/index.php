<?php
require "../bootstrap.php";

use Src\Controller\UserController;
use Src\Controller\AccountController;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$request_method = $_SERVER["REQUEST_METHOD"];

// cors
if("OPTIONS" == $request_method) {
    if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT");

    if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode('/', $uri);

// valid endpoint is 'users'
// everything else results in a 404 Not Found
if ('users' != $uri[1]) {
    header("HTTP/1.1 404 Not Found");
    exit();
}


// get user id
$user_id = isset($uri[2]) ? (int) $uri[2] : null;

// call controller
if (isset($uri[3]) && "accounts" == $uri[3]) {
    // get account id
    $account_id = isset($uri[4]) ? (int) $uri[4] : null;

    $controller = new AccountController($db_connection, $request_method, $account_id, $user_id);
} else {
    $controller = new UserController($db_connection, $request_method, $user_id);
}

// run
$controller->processRequest();

?>
