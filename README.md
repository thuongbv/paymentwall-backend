### Configure the application
Copy and edit the `.env` file and enter your enviroment variables there:

```
copy .env.example .env
```

Install the project dependencies and start the PHP server:

```
composer install
```

Create the database for the project:

```
mysql -uroot -p
CREATE DATABASE paymentwall CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
quit
```

Run the db seed
```
php dbseed.php
```

### Run the client application
```
cd public
php -S 127.0.0.1:8000
```
